import './App.css';
import { Routes, Route } from 'react-router-dom'
import { PhotoIndex } from './app/modules/photo/components/photo-index/PhotoIndex';
import { PhotoDetail } from './app/modules/photo/components/photo-detail/PhotoDetail';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<PhotoIndex />} />
        <Route path='/photo-detail/:id' element={<PhotoDetail />} />
        <Route component={<PhotoIndex />} />
      </Routes>
    </div>
  );
}

export default App;
