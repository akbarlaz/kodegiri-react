import rootReducer from './rootReducer';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux'
import { watcherSaga } from './rootSaga';

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];
const store = createStore(rootReducer, {}, applyMiddleware(...middleware));

sagaMiddleware.run(watcherSaga);

export default store;