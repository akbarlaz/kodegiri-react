import { all } from 'redux-saga/effects';
import { photoSaga } from '../modules/photo/redux/photoSaga';

export function* watcherSaga() {
    yield all([
        photoSaga(),
    ])
}