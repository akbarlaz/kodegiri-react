import { combineReducers } from 'redux'
import { photoState } from '../modules/photo/redux/photoReducer'
import { customModalState } from '../shared/components/custom-modal/redux/customModalReducer'

export default combineReducers({
    photoState,
    customModalState,
})