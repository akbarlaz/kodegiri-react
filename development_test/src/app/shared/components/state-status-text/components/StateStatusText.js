import React from 'react';
import { API_CALL_ERROR, API_CALL_FULLFILED, API_CALL_IDLE } from '../../../../constants/constant';
import styles from './StateStatusText.module.css';

export function StateStatusText({ status }) {

    if (status === API_CALL_IDLE || status === API_CALL_FULLFILED) {
        return
    }

    return (
        <div>
            <div className={styles.row}>
                {
                    status === API_CALL_ERROR ? (
                        <div>
                            Error
                        </div>
                    ) : (
                        <div>
                            Loading ...
                        </div>
                    )
                }
            </div>
        </div>
    );
}

