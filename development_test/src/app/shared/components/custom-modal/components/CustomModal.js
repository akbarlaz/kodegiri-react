import React from 'react'
import ReactDom from 'react-dom'
import styles from './CustomModal.module.css';

export function CustomModal({ open, onClose, children }) {
    if (!open) return null;
    return (
        <div onClick={onClose} className={styles.overlay}>
            <div
                onClick={(e) => {
                    e.stopPropagation();
                }}
                className={styles['modal-container']}
            >
                <div className={styles['modal-right']}>
                    <p className={styles['close-button']} onClick={onClose}>
                        X
                    </p>
                    {children}
                </div>
            </div>
        </div>
    );
}

