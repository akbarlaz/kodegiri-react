import { CUSTOM_MODAL_CLOSE, CUSTOM_MODAL_OPEN } from "../constants/constant";

const initialState = {
    isOpen: false,
    type: '',
};

export const customModalState = (
    state = initialState, action
) => {
    switch (action.type) {
        case CUSTOM_MODAL_OPEN:
            return {
                ...state,
                isOpen: true,
                type: action.payload.modalType,
            }
        case CUSTOM_MODAL_CLOSE:
            return {
                ...state,
                isOpen: false,
                type: '',
            }
        default:
            return state
    }
}