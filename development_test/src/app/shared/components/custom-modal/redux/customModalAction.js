import { CUSTOM_MODAL_CLOSE, CUSTOM_MODAL_OPEN } from "../constants/constant"

export const customModalOpen = ({ modalType }) => {
    return {
        type: CUSTOM_MODAL_OPEN,
        payload: {
            modalType
        }
    }
}

export const customModalClose = () => {
    return {
        type: CUSTOM_MODAL_CLOSE
    }
}
