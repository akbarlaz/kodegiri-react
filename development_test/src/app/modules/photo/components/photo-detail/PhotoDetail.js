import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { photoFetch, setSelectedPhoto } from '../../redux/photoAction';
import styles from './PhotoDetail.module.css';
import { EMPTY_DATA_TEXT } from '../../../../constants/constant';
import { Link, useParams } from 'react-router-dom';
import { StateStatusText } from '../../../../shared/components/state-status-text/components/StateStatusText';
import { PHOTO_MODAL_TYPE } from '../../constants/constant';
import { PhotoModal } from '../photo-modal/PhotoModal';
import { customModalClose, customModalOpen } from '../../../../shared/components/custom-modal/redux/customModalAction';
import { CustomModal } from '../../../../shared/components/custom-modal/components/CustomModal';

export function PhotoDetail() {
  let { id } = useParams();
  let photoState = useSelector((state) => state.photoState);
  let customModalState = useSelector((state) => state.customModalState);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(photoFetch({ id }));
  }, []);



  return (
    <div>
      <Link to={'/'} onClick={() => {
        dispatch(customModalClose())
        dispatch(setSelectedPhoto())
      }}>HOME</Link>
      <h4>PHOTO DETAIL</h4>
      <StateStatusText status={photoState.status} />
      {photoState.selectedPhoto ?
        (
          <div className={styles.row}>
            <div className={styles.card}>
              <img onClick={() => {
                dispatch(customModalOpen({ modalType: PHOTO_MODAL_TYPE }))
              }} src={photoState?.selectedPhoto?.urls?.thumb ?? ''} alt="Avatar" />
              <div className={styles.container}>
                <table>
                  <tbody>
                    <tr>
                      <td><b>Id</b></td>
                      <td>{photoState.selectedPhoto.id}</td>
                    </tr>
                    <tr>
                      <td><b>Description</b></td>
                      <td>{photoState.selectedPhoto.description}</td>
                    </tr>
                    <tr>
                      <td><b>Alt Description</b></td>
                      <td>{photoState.selectedPhoto.alt_description}</td>
                    </tr>
                    <tr>
                      <td><b>Downloads</b></td>
                      <td>{photoState.selectedPhoto.downloads}</td>
                    </tr>
                    <tr>
                      <td><b>Likes</b></td>
                      <td>{photoState.selectedPhoto.likes}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        ) :
        (
          <div className={styles.row}>
            <div className={styles.card}>
              <div className={styles.container}>
                <h4>{EMPTY_DATA_TEXT}</h4>
              </div>
            </div>
          </div>
        )
      }
      <CustomModal open={customModalState.isOpen}
        onClose={() => {
          dispatch(customModalClose())
        }}>
        {customModalState.type === PHOTO_MODAL_TYPE ? (<PhotoModal />) : null}
      </CustomModal>
    </div>
  );
}
