import React from 'react';
import { useDispatch } from 'react-redux';
import styles from './PhotoCard.module.css';
import { customModalOpen } from '../../../../shared/components/custom-modal/redux/customModalAction'
import { setSelectedPhoto } from '../../redux/photoAction';
import { Link } from 'react-router-dom';
import { PHOTO_MODAL_TYPE } from '../../constants/constant';

export function PhotoCard({ photo, innerRef, }) {
  const dispatch = useDispatch();

  return (
    <div>
      <div className={styles.row}>
        <div className={styles.card}>
          <img onClick={() => {
            dispatch(setSelectedPhoto(photo))
            dispatch(customModalOpen({ modalType: PHOTO_MODAL_TYPE }))
          }} src={photo.urls.thumb} alt="Avatar" />
          <div className={styles.container}>
            <h4 ><b>
              <Link to={`/photo-detail/${photo.id}`}>
                {photo.id}
              </Link>
            </b></h4>
            <h4 ref={innerRef}><b>{photo.description ?? 'No Description'}</b></h4>
          </div>
        </div>
      </div>
    </div>
  );
}
