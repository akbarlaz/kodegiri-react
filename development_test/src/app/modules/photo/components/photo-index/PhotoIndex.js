import React, { useEffect, useState, useRef, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { photoSearch, setSelectedPhoto } from '../../redux/photoAction';
import styles from './PhotoIndex.module.css';
import { PhotoCard } from '../photo-card/PhotoCard'
import { StateStatusText } from '../../../../shared/components/state-status-text/components/StateStatusText';
import useDebounce from '../../../../hooks/useDebounce';
import { API_CALL_PENDING, DEBOUNCE_DELAY, FIRST_PAGE } from '../../../../constants/constant';
import { PhotoModal } from '../photo-modal/PhotoModal';
import { CustomModal } from '../../../../shared/components/custom-modal/components/CustomModal';
import { customModalClose } from '../../../../shared/components/custom-modal/redux/customModalAction'
import { PHOTO_MODAL_TYPE } from '../../constants/constant';

export function PhotoIndex() {
  let photoState = useSelector((state) => state.photoState);
  let customModalState = useSelector((state) => state.customModalState);
  const dispatch = useDispatch();
  const [queryValue, setQueryValue] = useState('');
  const observer = useRef();
  const lastBookElementRef = useCallback(node => {
    if (photoState.status === API_CALL_PENDING) {
      return;
    }
    if (observer.current) {
      observer.current.disconnect();
    }
    observer.current = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting && photoState.hasNextPage) {
        dispatch(photoSearch({ perPage: 5, query: debouncedQuery, orderBy: 'latest', nextPage: true, page: photoState.page }))
      }
    })
    if (node) observer.current.observe(node)
  }, [photoState.status]);

  const debouncedQuery = useDebounce({ value: queryValue, delay: DEBOUNCE_DELAY });

  useEffect(() => {
    dispatch(photoSearch({ page: FIRST_PAGE, perPage: 5, query: debouncedQuery, orderBy: 'latest' }))
  }, [debouncedQuery]);



  return (
    <div>
      <div className={styles.row}>
        <input
          type={'search'}
          aria-label="Set increment amount"
          value={queryValue}
          onChange={(e) => setQueryValue(e.target.value)}
        >
        </input>
      </div>
      {
        photoState.list.map((el, index) =>
          <PhotoCard
            key={el.id}
            innerRef={photoState.list.length === index + 1 ? lastBookElementRef : null}
            photo={el}
          />
        )
      }
      <StateStatusText status={photoState.status} />
      <CustomModal open={customModalState.isOpen}
        onClose={() => {
          dispatch(setSelectedPhoto({}))
          dispatch(customModalClose())
        }}>
        {customModalState.type === PHOTO_MODAL_TYPE ? (<PhotoModal />) : null}
      </CustomModal>
    </div>
  );
}
