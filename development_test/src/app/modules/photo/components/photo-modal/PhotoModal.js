import React from 'react';
import { useSelector } from 'react-redux';
import styles from './PhotoModal.module.css';

export function PhotoModal() {
    let photoState = useSelector((state) => state.photoState);

    return (
        <div>
            <div className={styles.card}>
                <img src={photoState.selectedPhoto.urls.small} alt="Avatar" />
            </div>
        </div>
    );
}
