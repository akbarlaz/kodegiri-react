import { FETCH_PHOTO, SEARCH_PHOTO, SET_SELECTED_PHOTO } from "../constants/constant"

export const photoSearch = (query) => {
    return {
        type: SEARCH_PHOTO,
        query
    }
}

export const photoFetch = (query) => {
    return {
        type: FETCH_PHOTO,
        query
    }
}

export const setSelectedPhoto = (payload) => {
    return {
        type: SET_SELECTED_PHOTO,
        payload
    }
}
