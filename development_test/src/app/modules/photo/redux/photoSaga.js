import { takeLatest } from 'redux-saga/effects'
import { FETCH_PHOTO, SEARCH_PHOTO } from '../constants/constant';
import { handleSearchPhoto, handleFetchPhoto } from './photoHandler';

export function* photoSaga() {
    yield takeLatest(SEARCH_PHOTO, handleSearchPhoto)
    yield takeLatest(FETCH_PHOTO, handleFetchPhoto)
}