import { call, put } from 'redux-saga/effects';
import { API_CALL_ERROR, API_CALL_FULLFILED, API_CALL_PENDING, FIRST_PAGE } from '../../../constants/constant';
import { SET_PHOTO_API_CALL_STATUS, SET_PHOTO_LIST, SET_SELECTED_PHOTO } from '../constants/constant';
import { fetchPhoto, searchPhotos } from './photoRequest';

export function* handleSearchPhoto(action) {
    if (action.query.query.length === 0) {
        yield put({ type: SET_PHOTO_LIST, data: { "total": 0, "total_pages": 0, "results": [], status: API_CALL_FULLFILED, page: FIRST_PAGE } })
        return
    }
    yield put({ type: SET_PHOTO_API_CALL_STATUS, data: API_CALL_PENDING })
    try {
        let tmpAction = { ...action };
        if (tmpAction.query.nextPage) {
            tmpAction.query.page += 1;
        }
        const response = yield call(searchPhotos, tmpAction);
        yield put({ type: SET_PHOTO_LIST, data: { ...response.data, status: API_CALL_FULLFILED, page: tmpAction.query.page, } })
    } catch (error) {
        yield put({ type: SET_PHOTO_LIST, data: { "total": 0, "total_pages": 0, "results": [], status: API_CALL_ERROR, page: FIRST_PAGE } })
    }
}

export function* handleFetchPhoto(action) {
    yield put({ type: SET_PHOTO_API_CALL_STATUS, data: API_CALL_PENDING })
    try {
        const response = yield call(fetchPhoto, action);
        yield put({ type: SET_SELECTED_PHOTO, payload: { ...response.data } })
        yield put({ type: SET_PHOTO_API_CALL_STATUS, data: API_CALL_FULLFILED })
    } catch (error) {
        yield put({ type: SET_SELECTED_PHOTO, payload: undefined })
        yield put({ type: SET_PHOTO_API_CALL_STATUS, data: API_CALL_ERROR })
    }
}

