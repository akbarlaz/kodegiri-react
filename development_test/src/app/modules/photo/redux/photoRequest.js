import { API_URL, CLIENT_ID } from '../../../constants/constant';
import axios from 'axios';

export function searchPhotos(payload) {
    return axios.request({
        method: 'get',
        url: `${API_URL}/search/photos`,
        params: {
            client_id: CLIENT_ID,
            query: payload.query.query,
            page: payload.query.page,
            per_page: payload.query.perPage,
            order_by: payload.query.orderBy,
        }
    })
}

export function fetchPhoto(payload) {
    return axios.request({
        method: 'get',
        url: `${API_URL}/photos/${payload.query.id}`,
        params: {
            client_id: CLIENT_ID,
        }
    })
}