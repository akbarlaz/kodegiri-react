import { API_CALL_IDLE, FIRST_PAGE } from "../../../constants/constant";
import { SET_PHOTO_API_CALL_STATUS, SET_PHOTO_LIST, SET_SELECTED_PHOTO } from "../constants/constant";

const initialState = {
    status: API_CALL_IDLE,
    list: [],
    queryParam: {
        page: FIRST_PAGE,
        perPage: 5,
        query: '',
        orderBy: 'latest',
    },
    total: 0,
    totalPages: 0,
    hasNextPage: false,
    selectedPhoto: undefined
};

export const photoState = (
    state = initialState, action
) => {
    switch (action.type) {
        case SET_PHOTO_LIST:
            const totalPages = action.data.total_pages + 1;
            return {
                ...state,
                page: action.data.page,
                list:
                    action.data.page === FIRST_PAGE ? [...action.data.results] :
                        [...new Set([...state.list, ...action.data.results])],
                total: action.data.total,
                totalPages: totalPages,
                status: action.data.status,
                hasNextPage: action.data.page < totalPages,
            }
        case SET_PHOTO_API_CALL_STATUS:
            return {
                ...state,
                status: action.data,
            }
        case SET_SELECTED_PHOTO:
            return {
                ...state,
                selectedPhoto: action.payload,
            }
        default:
            return state
    }
}