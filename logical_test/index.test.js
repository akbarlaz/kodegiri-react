const isPalindrom = require("./index");
test("case 1: Palindrom function should return true", () => {
    expect(isPalindrom('992299')).toBe(true);
    expect(isPalindrom('982289')).toBe(true);
    expect(isPalindrom('22022022')).toBe(true);
});

test("case 2: Palindrom function should return false", () => {
    expect(isPalindrom('092289')).toBe(false);
});