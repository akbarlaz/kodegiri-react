const DATA_TO_FILTER = [
    '092289',
    '992299',
    '12291',
    '982289',
    '22022022',
    '2301',
    '2013',
    '1001',
    '756564',
    '1011',
    '766567',
    '756546',
    '2002',
    '91019',
    '765567',
]

function isPalindrom(value) {
    for (let i = 0; i < Math.floor(value.length / 2); i++) {
        if (value[i] !== value[value.length - i - 1]) {
            return false;
        }
    }
    return true;
}

function main() {
    const expectedData = DATA_TO_FILTER.filter((el) => isPalindrom(el));
    console.log(expectedData);
}

main();

module.exports = isPalindrom;